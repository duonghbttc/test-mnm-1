#include <conio.h>
#include <iostream>
using namespace std;


struct ThreexThree
{
	int O[3][3];
};

struct StatusNode
{
	int O[3][3];
	int different;
	StatusNode* parent;
	StatusNode* next;
};

int abs(int n)
{
	if (n >= 0) return n;
	else return (-n);
}

void Give(StatusNode* &status, ThreexThree txt)
{
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			status->O[i][j] = txt.O[i][j];
		}
	}
}

void GiveEx(StatusNode* &status1, StatusNode* &status2)
{
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			status1->O[i][j] = status2->O[i][j];
		}
	}
}

bool Match(StatusNode* status, ThreexThree txt)
{
	int count = 0;
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			if (status->O[i][j] == txt.O[i][j])
			{
				count ++;
			}
		}
	}
	if (count == 9) return true;
	else return false;
}

// Ham danh gia tinh tong cac buoc di giua 2 trang thai ( dung cho thuat tim kiem theo ham danh gia )
int MatchEx(StatusNode* status, ThreexThree txt)
{
	int count = 0;
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			for (int m = 0; m <= 2; m++)
				for (int n = 0; n <= 2; n++)
					if (status->O[i][j] == txt.O[m][n] && status->O[i][j] != 0)
					{
						count += abs(i - m) + abs(j - n);
					}
		}
	}
	return count;
}

struct Stack
{
	StatusNode* top;
};

void InitS(Stack &s)
{
	s.top = NULL;
}

bool ExistS(Stack s)
{
	if (s.top == NULL) return false;
	else return true;
}

void PushS(Stack &s, StatusNode* &status)
{
	if (ExistS(s) == false)
	{
		s.top = status;
	}
	else
	{
		status->next = s.top;
		s.top = status;
	}
}

StatusNode* PopS(Stack &s)
{
	StatusNode* p;
	if (ExistS(s) == true)
	{
		p = s.top;
		s.top = (s.top)->next;
		p->next = NULL;
		return p;
	}
	return NULL;
}

struct Queue
{
	StatusNode* front;
	StatusNode* rear;
};

void InitQ(Queue &q)
{
	q.front = NULL;
	q.rear = NULL;
}

bool ExistQ(Queue q)
{
	if (q.front == NULL) return false;
	else return true;
}

void EnQ(Queue &q, StatusNode* &status)
{
	if (ExistQ(q) == false)
	{
		q.front = q.rear = status;
	}
	else
	{
		(q.rear)->next = status;
		q.rear = status;
	}
}

StatusNode* DeQ(Queue &q)
{
	StatusNode* p;
	if (ExistQ(q) == true)
	{
		p = q.front;
		q.front = (q.front)->next;
		p->next = NULL;
		return p;
	}
	return NULL;
}

struct List
{
	StatusNode* head;	
};

void InitL(List &l)
{
	l.head = NULL;
}

bool ExistL(List l)
{
	if (l.head ==  NULL) return false;
	else return true;
}

void InsertL(List &l, StatusNode* &status)
{
	StatusNode* p;
	p = l.head;
	if (ExistL(l) == false)
	{
		l.head = status;
	}
	else if (status->different < l.head->different)
	{
		status->next = l.head;
		l.head = status;
	}
	else
	{
		while (p->next != NULL && (p->next)->different < status->different)
		{
			p = p->next;
		}
		status->next = p->next;
		p->next = status;
	}
}

StatusNode* DetachL(List &l)
{
	StatusNode* p;
	if (ExistL(l) == true)
	{
		p = l.head;
		l.head = (l.head)->next;
		p->next = NULL;
		return p;
	}
	return NULL;
}

void ChessWidthSearch(ThreexThree F, ThreexThree E)
{
	int NodesinQueue = 0;
	int MaxNodeinQueue = 0;
	Queue q;
	InitQ(q);
	StatusNode* Tree = new StatusNode;
	Give(Tree, F);
	Tree->different = MatchEx(Tree, E);
	Tree->parent = NULL;
	Tree->next = NULL;
	EnQ(q, Tree);
	NodesinQueue++;
	while(true)
	{
		if (ExistQ(q) == false)
		{
			cout << "Tim kiem theo chieu rong that bai !";
			break;
		}
		StatusNode* DeQresult = DeQ(q);
		NodesinQueue--;
		if (Match(DeQresult, E) == true)
		{
			int cost = 0;
			cout << "Ket qua tim kiem theo chieu rong ( tu duoi len tren ) : " << endl;
			while (DeQresult != NULL)
			{
				cout << "-> - - - " << endl;
				for (int i = 0; i <= 2; i++)
				{
					cout << "   ";
					for (int j = 0; j <= 2; j++)
					 {
					 	cout << DeQresult->O[i][j] << " ";
					 }
					 cout << endl;
				}
				DeQresult = DeQresult->parent;
				cost++;
			}
			cout << "Chi phi : " << cost - 1 << endl;
			cout << "So Node cao nhat trong Queue : " << MaxNodeinQueue;
			break;
		}
		else
		{
			for (int i = 0; i <= 2; i++)
			{
				for (int j = 0; j <= 2; j++)
				{
					if (DeQresult->O[i][j] == 0)
					{
						if (i > 0)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DeQresult);
							newnode->O[i-1][j] = DeQresult->O[i][j];
							newnode->O[i][j] = DeQresult->O[i-1][j];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DeQresult;
							newnode->next = NULL;
							EnQ(q, newnode);
							NodesinQueue++;
						}
						if (i < 2)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DeQresult);
							newnode->O[i+1][j] = DeQresult->O[i][j];
							newnode->O[i][j] = DeQresult->O[i+1][j];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DeQresult;
							newnode->next = NULL;
							EnQ(q, newnode);
							NodesinQueue++;
						}
						if (j > 0)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DeQresult);
							newnode->O[i][j-1] = DeQresult->O[i][j];
							newnode->O[i][j] = DeQresult->O[i][j-1];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DeQresult;
							newnode->next = NULL;
							EnQ(q, newnode);
							NodesinQueue++;
						}
						if (j < 2)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DeQresult);
							newnode->O[i][j+1] = DeQresult->O[i][j];
							newnode->O[i][j] = DeQresult->O[i][j+1];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DeQresult;
							newnode->next = NULL;
							EnQ(q, newnode);
							NodesinQueue++;
						}
					}
				}
			}
		}
		if (NodesinQueue > MaxNodeinQueue)
		{
			MaxNodeinQueue = NodesinQueue;
		}
	}
}

void ChessDeepSearch(ThreexThree F, ThreexThree E)
{
	int NodesinStack = 0;
	int MaxNodeinStack = 0;
	Stack s;
	InitS(s);
	StatusNode* Tree = new StatusNode;
	Give(Tree, F);
	Tree->different = MatchEx(Tree, E);
	Tree->parent = NULL;
	Tree->next = NULL;
	PushS(s, Tree);
	NodesinStack++;
	while(true)
	{
		if (ExistS(s) == false)
		{
			cout << "Tim kiem theo chieu sau that bai !";
			break;
		}
		StatusNode* PopSresult = PopS(s);
		NodesinStack--;
		if (Match(PopSresult, E) == true)
		{
			int cost = 0;
			cout << "Ket qua tim kiem theo chieu sau ( tu duoi len tren ) : " << endl;
			while (PopSresult != NULL)
			{
				cout << "-> - - - " << endl;
				for (int i = 0; i <= 2; i++)
				{
					cout << "   ";
					for (int j = 0; j <= 2; j++)
					 {
					 	cout << PopSresult->O[i][j] << " ";
					 }
					 cout << endl;
				}
				PopSresult = PopSresult->parent;
				cost++;
			}
			cout << "Chi phi : " << cost - 1 << endl;
			cout << "So Node cao nhat trong Stack : " << MaxNodeinStack;
			break;
		}
		else
		{
			for (int i = 0; i <= 2; i++)
			{
				for (int j = 0; j <= 2; j++)
				{
					if (PopSresult->O[i][j] == 0)
					{
						if (i > 0)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, PopSresult);
							newnode->O[i-1][j] = PopSresult->O[i][j];
							newnode->O[i][j] = PopSresult->O[i-1][j];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = PopSresult;
							newnode->next = NULL;
							PushS(s, newnode);
							NodesinStack++;
						}
						if (i < 2)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, PopSresult);
							newnode->O[i+1][j] = PopSresult->O[i][j];
							newnode->O[i][j] = PopSresult->O[i+1][j];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = PopSresult;
							newnode->next = NULL;
							PushS(s, newnode);
							NodesinStack++;
						}
						if (j > 0)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, PopSresult);
							newnode->O[i][j-1] = PopSresult->O[i][j];
							newnode->O[i][j] = PopSresult->O[i][j-1];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = PopSresult;
							newnode->next = NULL;
							PushS(s, newnode);
							NodesinStack++;
						}
						if (j < 2)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, PopSresult);
							newnode->O[i][j+1] = PopSresult->O[i][j];
							newnode->O[i][j] = PopSresult->O[i][j+1];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = PopSresult;
							newnode->next = NULL;
							PushS(s, newnode);
							NodesinStack++;
						}
					}
				}
			}
		}
		if (NodesinStack > MaxNodeinStack)
		{
			MaxNodeinStack = NodesinStack;
		}
	}
}

void ChessBestFirstSearch(ThreexThree F, ThreexThree E)
{
	int NodesinList = 0;
	int MaxNodeinList = 0;
	List l;
	InitL(l);
	StatusNode* Tree = new StatusNode;
	Give(Tree, F);
	Tree->different = MatchEx(Tree, E);
	Tree->parent = NULL;
	Tree->next = NULL;
	InsertL(l, Tree);
	NodesinList++;
	while(true)
	{
		if (ExistL(l) == false)
		{
			cout << "Tim kiem theo phuong an toi uu that bai !";
			break;
		}
		StatusNode* DetachLresult = DetachL(l);
		NodesinList--;
		if (Match(DetachLresult, E) == true)
		{
			int cost = 0;
			cout << "Ket qua tim kiem theo phuong an toi uu ( tu duoi len tren ) : " << endl;
			while (DetachLresult != NULL)
			{
				cout << "-> - - - " << endl;
				for (int i = 0; i <= 2; i++)
				{
					cout << "   ";
					for (int j = 0; j <= 2; j++)
					 {
					 	cout << DetachLresult->O[i][j] << " ";
					 }
					 cout << endl;
				}
				DetachLresult = DetachLresult->parent;
				cost++;
			}
			cout << "Chi phi : " << cost - 1 << endl;
			cout << "So Node cao nhat trong Linked-List : " << MaxNodeinList;
			break;
		}
		else
		{
			for (int i = 0; i <= 2; i++)
			{
				for (int j = 0; j <= 2; j++)
				{
					if (DetachLresult->O[i][j] == 0)
					{
						if (i > 0)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DetachLresult);
							newnode->O[i-1][j] = DetachLresult->O[i][j];
							newnode->O[i][j] = DetachLresult->O[i-1][j];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DetachLresult;
							newnode->next = NULL;
							InsertL(l, newnode);
							NodesinList++;
						}
						if (i < 2)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DetachLresult);
							newnode->O[i+1][j] = DetachLresult->O[i][j];
							newnode->O[i][j] = DetachLresult->O[i+1][j];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DetachLresult;
							newnode->next = NULL;
							InsertL(l, newnode);
							NodesinList++;
						}
						if (j > 0)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DetachLresult);
							newnode->O[i][j-1] = DetachLresult->O[i][j];
							newnode->O[i][j] = DetachLresult->O[i][j-1];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DetachLresult;
							newnode->next = NULL;
							InsertL(l, newnode);
							NodesinList++;
						}
						if (j < 2)
						{
							StatusNode* newnode = new StatusNode;
							GiveEx(newnode, DetachLresult);
							newnode->O[i][j+1] = DetachLresult->O[i][j];
							newnode->O[i][j] = DetachLresult->O[i][j+1];
							newnode->different = MatchEx(newnode, E);
							newnode->parent = DetachLresult;
							newnode->next = NULL;
							InsertL(l, newnode);
							NodesinList++;
						}
					}
				}
			}
		}
		if (NodesinList > MaxNodeinList)
		{
			MaxNodeinList = NodesinList;
		}
	}
}

int main()
{
	ThreexThree First, End;
	int choose;
	cout << "Nhap vao thu trang thai dau theo thu tu ( vi tri rong nhap 0 ) : " << endl;
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			cout << " Vi tri [ " << i << " ] [ " << j << " ] = ";
			cin >> First.O[i][j];
		}
	}
	cout << "Nhap vao thu trang thai ket thuc theo thu tu ( vi tri rong nhap 0 ) : " << endl;
	for (int i = 0; i <= 2; i++)
	{
		for (int j = 0; j <= 2; j++)
		{
			cout << " Vi tri [ " << i << " ] [ " << j << " ] = ";
			cin >> End.O[i][j];
		}
	}
choose:
	cout << endl << "1. Tim kiem theo chieu rong " << endl << "2. Tim kiem theo chieu sau " << endl << "3. Tim kiem theo ham danh gia " << endl << "Nhap vao so thu tu cua thuat toan su dung : ";
	cin >> choose;
	if (choose == 1)
	{
		ChessWidthSearch(First, End);
	}
	if (choose == 2)
	{
		ChessDeepSearch(First, End);
	}
	if (choose == 3)
	{
		ChessBestFirstSearch(First, End);
	}
	getch();
    return 0;
}
